//
db.fruits.aggregate(
	[
		{$match:{onSale: {$eq:true}}},//first stage
		{$group:{_id:null, fruitsOnSale:{$sum:1}}}//sencond
	]
);
//
db.fruits.aggregate(
	[
		{$match:{stock: {$gte:20}}},//first stage
		{$group:{_id:null, enoughStock:{$sum:1}}}//sencond

	]
);
//
db.fruits.aggregate(
	[
		{$match:{onSale: {$eq:true}}},//first stage
		{$group:{_id:"$supplier_id", averagePrice:{$avg:"$price"}}}//sencond

	]
);
//
db.fruits.aggregate(
	[
		{$group:{_id:"$supplier_id", maxPrice:{$max:"$price"}}}//sencond

	]
);
//
db.fruits.aggregate(
	[
		{$group:{_id:"$supplier_id", minPrice:{$min:"$price"}}}//sencond

	]
);

//////////////////////////DATA
db.fruits.insertMany(
[{
			name : "Apple",
			color : "Red",
			stock : 20,
			price: 40,
			supplier_id : 1,
			onSale : true,
			origin: [ "Philippines", "US" ]
		},

		{
			name : "Banana",
			color : "Yellow",
			stock : 15,
			price: 20,
			supplier_id : 2,
			onSale : true,
			origin: [ "Philippines", "Ecuador" ]
		},

		{
			name : "Kiwi",
			color : "Green",
			stock : 25,
			price: 50,
			supplier_id : 1,
			onSale : true,
			origin: [ "US", "China" ]
		},

		{
			name : "Mango",
			color : "Yellow",
			stock : 10,
			price: 120,
			supplier_id : 2,
			onSale : false,
			origin: [ "Philippines", "India" ]
		}]

);

//////////////////////////////////